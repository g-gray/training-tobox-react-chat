'use strict';

var users = {};

var add = function (name) {
    if (!name || users[name]) {
        return false;
    }

    users[name] = {name: name};

    return true;
};

var remove = function (name) {
    if (users[name]) {
        delete users[name];
    }
};

var get = function () {
    return Object.keys(users).map(function (name) {
        return name;
    });
};

var createGuest = function () {
    var name,
        nextUserId = 1;

    do {
        name = 'Guest ' + nextUserId;
        nextUserId++;
    } while (!add(name));

    return name;
};

module.exports = {
    add: add,
    remove: remove,
    get: get,
    createGuest: createGuest
};
