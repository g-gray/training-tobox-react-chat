'use strict';
var Users = require('./users.js');

// Export function for listening to the socket
module.exports = function (socket) {
    var name = Users.createGuest();

    console.log(Users.get());

    // Send the new user their name and a list of users
    socket.emit('init', {
        name: name,
        users: Users.get()
    });

    // Notify other clients that a new user has joined
    socket.broadcast.emit('user:join', {
        name: name
    });

    // Validate a user's name change, and broadcast it on success
    socket.on('user:changeName', function (data, callback) {
        if (Users.add(data.name)) {
            var oldName = name;

            Users.remove(oldName);
            name = data.name;
            socket.broadcast.emit('user:changeName', {
                oldName: oldName,
                newName: name
            });

            callback(true);
        } else {
            callback(false);
        }
    });

    // Broadcast a user's message to other users
    socket.on('user:sendMessage', function (data) {
        console.log(data);
        socket.broadcast.emit('user:sendMessage', {
            name: name,
            text: data.text
        });
    });

    // Clean up when a user leaves, and broadcast it to other users
    socket.on('disconnect', function () {
        socket.broadcast.emit('user:left', {
            name: name
        });
        Users.remove(name);

        console.log('Client disconnected.');
    });
};
