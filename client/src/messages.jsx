import React from 'react';

import Message from './message.jsx';

export default class Messages extends React.Component {
    render() {
        return (
            <div className="messages">
                <h3>Messages</h3>

                {this.props.messages.map((message, idx) =>  (
                    <Message key={idx} name={message.name} text={message.text}/>
                ))}
            </div>
        );
    }
}
