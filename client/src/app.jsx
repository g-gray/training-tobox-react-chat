import React from 'react';
import SocketIo from 'socket.io';

import Users from './users.jsx';
import Messages from './messages.jsx';
import NewMessageForm from './new-message-form.jsx';
import ChangeNameForm from './change-name-form.jsx';

var socket = SocketIo();

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {users: [], messages: [], name: ''};

        this.handleSendMessage = this.handleSendMessage.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
    }

    componentDidMount() {
        socket.on('init', this.handleInit.bind(this));
        socket.on('user:join', this.handleUserJoin.bind(this));
        socket.on('user:left', this.handleUserLeft.bind(this));
        socket.on('user:sendMessage', this.handleUserSendMessage.bind(this));
        socket.on('user:changeName', this.handleUserChangeName.bind(this));
    }

    handleInit(data) {
        var {users, name} = data;

        this.setState({users, name});
    }

    handleUserJoin(data) {
        var {users, messages} = this.state;
        var {name} = data;

        users.push(name);
        messages.push({
            name: 'BOT',
            text: name + ' joined'
        });
        this.setState({users, messages});
    }

    handleUserLeft(data) {
        var {users, messages} = this.state;
        var {name} = data;
        var index = users.indexOf(name);

        users.splice(index, 1);
        messages.push({
            name: 'BOT',
            text: name + ' left'
        });
        this.setState({users, messages});
    }

    handleUserSendMessage(message) {
        var {messages} = this.state;

        messages.push(message);
        this.setState({messages});
    }

    handleUserChangeName(data) {
        var {oldName, newName} = data;
        var {users, messages} = this.state;
        var index = users.indexOf(oldName);

        users.splice(index, 1, newName);
        messages.push({
            name: 'BOT',
            text: 'Change Name : ' + oldName + ' ==> ' + newName
        });
        this.setState({users, messages});
    }

    handleSendMessage(message) {
        var {messages} = this.state;

        messages.push(message);
        this.setState({messages});
        socket.emit('user:sendMessage', message);
    }

    handleChangeName(newName) {
        var oldName = this.state.name;

        socket.emit('user:changeName', {name: newName}, (result) => {

            // TODO Make beautiful area with errors
            if (!result) {
                return alert('There was an error changing your name');
            }

            var {users} = this.state;
            var index = users.indexOf(oldName);

            users.splice(index, 1, newName);
            this.setState({users, name: newName});
        });
    }

    render() {
        return (
            <div className="container">

                <div className="row">

                    <div className="col-md-4">
                        <ChangeNameForm onChangeName={this.handleChangeName} name={this.state.name}/>
                        <Users users={this.state.users}/>
                    </div>

                    <div className="col-md-8">
                        <Messages messages={this.state.messages}/>
                    </div>

                </div>

                <div className="row">
                    <div className="col-md-12">
                        <NewMessageForm onSendMessage={this.handleSendMessage} name={this.state.name}/>
                    </div>
                </div>

            </div>
        );
    }
}

React.render(<App/>, document.getElementById('app'));
