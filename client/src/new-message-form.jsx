import React from 'react';

export default class NewMessageForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {text: ''};
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();

        var message = {
            name: this.props.name,
            text: this.state.text
        };

        this.props.onSendMessage(message);
        this.setState({text: ''});
    }

    handleChange(e) {
        this.setState({text: e.target.value});
    }

    handleKeyPress(e) {
        if (e.keyCode() === 13) {
            this.handleSubmit(e);
        }
    }

    render() {
        return (
            <div className="new-message-form">
                <h3>Write New Message</h3>

                <form onSubmit={this.handleSubmit}>
                    <textarea type="text" value={this.state.text}
                              onChange={this.handleChange} onKeyPress={this.handleKeyPress}/>
                    <button type="sumbit">Send</button>
                </form>
            </div>
        );
    }
}
