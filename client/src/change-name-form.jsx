import React from 'react';

export default class ChangeNameForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {name: this.props.name};
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();

        this.props.onChangeName(this.state.name);
        this.setState({name: ''});
    }

    handleChange(e) {
        this.setState({name: e.target.value});
    }

    render() {
        return (
            <div className="change-name-form">
                <h3> Change Name </h3>

                <form onSubmit={this.handleSubmit}>
                    <input type="text" value={this.state.name} onChange={this.handleChange}/>
                    <button type="sumbit">Change</button>
                </form>
            </div>
        );
    }
}
