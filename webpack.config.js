var path = require('path');

var sourceDir = path.join(__dirname, '/client/src'),
    buildDir = path.join(__dirname, '/client/build');

var Webpack = require('webpack'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    HtmlWebpackPlugin = require('html-webpack-plugin');

var packageJSON = require(path.join(__dirname, '/package.json')),
    vendors = Object
        .keys(packageJSON.dependencies)
        .filter(function (vendor) {
            return vendor !== 'express';
        });

var config = {
    entry: {
        vendor: vendors.concat(['bootstrap-css']),
        app: [path.join(sourceDir, '/app.jsx')]
    },
    output: {
        path: buildDir,
        filename: '[name].js'
    },
    module: {
        noParse: vendors,
        loaders: [
            {test: /\.jsx?$/, loader: 'babel', include: [sourceDir]},
            {test: /\.css$/, loader: ExtractTextPlugin.extract('css')},
            {test: /\.scss/, loader: ExtractTextPlugin.extract('css!sass')},
            {test: /\.woff(2)?(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/font-woff"},
            {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/octet-stream"},
            {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file"},
            {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=image/svg+xml"},
            {test: /\.(png|gif|jpg|jpeg|bmp)(\?.*$|$)/, loader: 'url?limit=100000'}
        ]
    },
    plugins: [
        new Webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.js'),
        new ExtractTextPlugin('[name].css', {allChunks: true}),
        new Webpack.ProvidePlugin({$: 'jquery', jQuery: 'jquery', 'window.jQuery': 'jquery'}),
        new HtmlWebpackPlugin({filename: 'index.html', template: path.join(sourceDir, 'index.html')})
    ],
    resolve: {
        alias: {
            'socket.io': path.join(__dirname, '/node_modules/socket.io/node_modules/socket.io-client/socket.io.js'),
            'bootstrap-css': path.join(__dirname, '/node_modules/bootstrap/dist/css/bootstrap.min.css')
        }
    },
    node: {
        fs: 'empty'
    }
};

module.exports = config;
